#!/bin/bash

set -ex

# Set timezone
sudo timedatectl set-timezone Asia/Taipei

# Install necessare pacakges
sudo apt update
sudo apt upgrade -y
sudo apt install -y build-essential git curl

# Put some personal files
mkdir -p $HOME/usr
mkdir -p $HOME/usr/bin
cd $HOME/usr

git clone --recursive https://gitlab.com/yen3/dotfiles rc
cd $HOME/usr/rc
./install_binary.sh env neovim go docker-compose ctop pyenv diff-so-fancy docker-binary docker
./install_dotfiles.sh

cd $HOME
rm -rf shutdown.sh test_file VBoxGuestAdditions.iso
